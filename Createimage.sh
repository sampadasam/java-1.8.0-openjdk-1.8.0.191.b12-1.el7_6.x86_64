declare -r IMAGE_NAME="sampa/java-docker-build-tutorial"
declare -r IMAGE_TAG="latest"

echo "Building image '$IMAGE_NAME:$IMAGE_TAG'"
docker build -t $IMAGE_NAME:$IMAGE_TAG .
#docker save $IMAGE_NAME:$IMAGE_TAG > $IMAGE_NAME-$IMAGE_TAG.tar